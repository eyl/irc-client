# IRC Client

A primitive form of communication.

## Design

We target the Wayland protocol. Therefore, this software only runs on modern
Linux distributions. Everything is a work in progress.

## TODO

- [ ] Cairo font scaling to DPI
- [ ] Parse host names properly

## License

All code is licensed under GPLv3+.
